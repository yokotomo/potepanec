class Potepan::ProductsController < ApplicationController
  MAX_DISPLAY_NUMBER_OF_RELATED_PRODUCTS = 4
  def show
    @product = Spree::Product.includes(product_properties: :property).friendly.find(params[:id])
    @related_products = Spree::Product.includes_images_and_price.
      related_products_without_duplication(@product).
      limit(MAX_DISPLAY_NUMBER_OF_RELATED_PRODUCTS)
  end
end

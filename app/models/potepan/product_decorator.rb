module Potepan::ProductDecorator
  def self.prepended(base)
    base.scope :includes_images_and_price, -> { base.includes(master: [:images, :default_price]) }
    base.scope :related_products_without_duplication, ->(product) {
      base.joins(:taxons).
        where(spree_taxons: { id: product.taxons.ids }).
        where.not(id: product.id).
        distinct
    }
  end

  Spree::Product.prepend self
end

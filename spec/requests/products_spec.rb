require 'rails_helper'

RSpec.describe "Products page", type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe "#show with friendly id" do
    before do
      get potepan_product_path(product)
    end

    it "responds successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status(200)
    end

    it "renders the correct template" do
      expect(response).to render_template("potepan/products/show")
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq product
    end

    it "has product name in response" do
      expect(response.body).to include(product.name)
    end

    it "has product price in response" do
      expect(response.body).to include(product.display_price.to_s)
    end

    it "has product description in response" do
      expect(response.body).to include(product.description)
    end
  end

  describe "#show" do
    before do
      get potepan_product_path(product.id)
    end

    it "responds successfully" do
      expect(response).to be_successful
      expect(response).to have_http_status(200)
    end

    it "renders the correct template" do
      expect(response).to render_template("potepan/products/show")
    end

    it "has product name in response" do
      expect(response.body).to include(product.name)
    end

    it "has product price in response" do
      expect(response.body).to include(product.display_price.to_s)
    end

    it "has product description in response" do
      expect(response.body).to include(product.description)
    end
  end

  describe "#show with invalid id" do
    subject { -> { get potepan_product_path('abc') } }

    it { is_expected.to raise_error ActiveRecord::RecordNotFound }
  end
end

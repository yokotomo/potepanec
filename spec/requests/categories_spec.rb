require 'rails_helper'

RSpec.describe "Categories page", type: :request do
  let(:taxon) { create(:taxon) }

  describe "#show" do
    context "with valid id" do
      it "responds successfully" do
        get potepan_category_path(taxon.id)
        expect(response).to be_successful
        expect(response).to have_http_status(200)
      end
    end

    context "with invalid id" do
      subject { -> { get potepan_category_path('abc') } }

      it { is_expected.to raise_error ActiveRecord::RecordNotFound }
    end
  end
end

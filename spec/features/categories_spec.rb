require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  # Taxonomy
  given(:category) { create(:taxonomy, name: "Category") }
  # Taxon
  given(:bag) { create(:taxon, name: "bag", taxonomy: category, parent: category.root) }
  given(:clothing) { create(:taxon, name: "clothing", taxonomy: category, parent: category.root) }
  given(:shirt) { create(:taxon, name: "shirt", taxonomy: category, parent: clothing) }
  # Product
  given!(:ruby_bag) { create(:product, name: "ruby_bag", price: 10, taxons: [bag]) }
  given!(:ruby_shirt) { create(:product, name: "ruby_shirt", price: 20, taxons: [shirt]) }

  scenario "user visits a category page" do
    visit potepan_category_path(shirt.id)
    expect(page).to have_title shirt.name
    within '.side-nav' do
      expect(page).to have_content category.name
      expect(page).to have_content bag.name
      expect(page).to have_content clothing.name
      expect(page).to have_content shirt.name
    end
    within '.productBox' do
      expect(page).to have_content ruby_shirt.name
      expect(page).to have_content ruby_shirt.display_price
    end
  end

  scenario "user moves from a category page to another category page" do
    visit potepan_category_path(shirt.id)
    click_on bag.name
    expect(page).to have_current_path(potepan_category_path(bag.id))
    expect(page).to have_title bag.name
    within '.side-nav' do
      expect(page).to have_content category.name
      expect(page).to have_content bag.name
      expect(page).to have_content clothing.name
      expect(page).to have_content shirt.name
    end
    within '.productBox' do
      expect(page).to have_content ruby_bag.name
      expect(page).to have_content ruby_bag.display_price
    end
  end

  scenario "user cliks on a product name to go to its product details page" do
    visit potepan_category_path(shirt.id)
    click_on ruby_shirt.name
    expect(page).to have_current_path(potepan_product_path(ruby_shirt.id))
    expect(page).to have_title ruby_shirt.name
    expect(page).to have_content ruby_shirt.name
    expect(page).to have_content ruby_shirt.display_price
    expect(page).to have_content ruby_shirt.description
  end
end

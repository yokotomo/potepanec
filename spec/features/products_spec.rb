require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:bag) { create(:taxon, name: "bag") }
  given(:ruby) { create(:taxon, name: "ruby") }
  given(:rails) { create(:taxon, name: "rails") }
  given!(:rails_tote) { create(:product, name: "rails_tote", price: 10, taxons: [bag, rails]) }
  given!(:ruby_rails_bag) { create(:product, name: "ruby_rails_bag", price: 20, taxons: [bag, ruby, rails]) }
  given!(:ruby_shirt) { create(:product, name: "ruby_shirt", price: 30, taxons: [ruby]) }
  given!(:rails_product_1) { create(:product, name: "rails_product_1", price: 40, taxons: [rails]) }
  given!(:rails_product_2) { create(:product, name: "rails_product_2", price: 50, taxons: [rails]) }
  given!(:rails_product_3) { create(:product, name: "rails_product_3", price: 60, taxons: [rails]) }
  given!(:rails_product_4) { create(:product, name: "rails_product_4", price: 70, taxons: [rails]) }

  scenario "user visits a product page" do
    visit potepan_product_path(rails_tote)
    expect(page).to have_title rails_tote.name
    within '.singleProduct' do
      expect(page).to have_content rails_tote.name
      expect(page).to have_content rails_tote.display_price
      expect(page).to have_content rails_tote.description
    end
    within '.productsContent' do
      # exclude different taxons
      expect(page).not_to have_content ruby_shirt.name
      expect(page).not_to have_content ruby_shirt.display_price
      # exclude the main product itself
      expect(page).not_to have_content rails_tote.name
      expect(page).not_to have_content rails_tote.display_price
      # limit(4)
      expect(page).to have_content ruby_rails_bag.name, count: 1
      expect(page).to have_content ruby_rails_bag.display_price, count: 1
      expect(page).to have_content rails_product_1.name
      expect(page).to have_content rails_product_1.display_price
      expect(page).to have_content rails_product_2.name
      expect(page).to have_content rails_product_2.display_price
      expect(page).to have_content rails_product_3.name
      expect(page).to have_content rails_product_3.display_price
      expect(page).not_to have_content rails_product_4.name
      expect(page).not_to have_content rails_product_4.display_price
    end
  end

  scenario "user clicks on a related_product name to go the related_product page" do
    visit potepan_product_path(rails_tote)
    click_on ruby_rails_bag.name
    expect(page).to have_current_path(potepan_product_path(ruby_rails_bag.id))
  end

  scenario "user moves from a product page to a category page" do
    visit potepan_product_path(rails_tote)
    click_on '一覧ページへ戻る'
    expect(page).to have_current_path(potepan_category_path(rails_tote.taxons.first.id))
  end
end
